# Contrôle 2e session -- Programmation Python
**durée 1h30 -- Aucun document autorisé**

## Les listes -- 8 pts

Dites ce que vous savez des listes Python. Etayez vos propos d'exemples. Précisez en particulier la différence avec les tuples.

## Fonction mystère -- 12 pts

La fonction prédéfinie Python `ord` donne le code ascii d'un caractère. Ainsi `ord('a')` vaut 97. On considère la fonction suivante :

```python
def mystere(w, v):
    tab = [0] * 26
    for c in w:
        tab[ord(c) - ord('a')] += 1
    for c in v:
        tab[ord(c) - ord('a')] -= 1
    for e in tab:
        if e != 0:
            return False
    return True
```

### Exemples d'exécution (3 pts)

Que font les appels suivants :

- `mystere('chien', 'niche')`
- `mystere('python', 'python')`
- `mystere('bleu', 'vert')`

### Que fait cette fonction ? (3 pts)

Expliquer ce que fait cette fonction et comment elle le fait.

### Correction (3 pts)

Le code ascii du caractère souligné (`'_'`) vaut 95.

- Que fait cet appel : `mystere('_', 'y')` ? Expliquez. 
- En déduire une condition sur les paramètres de la fonction `mystere` pour garantir un comportement correct.

### Autre proposition (3 pts)

Proposez un autre algorithme de la fonction `mystere` et son écriture en Python.