LIBRE = '.'
REINE = 'R'
EN_PRISE = 'x'

# Les huit directions depuis une case de la grille
DIRECTIONS = (1, 1), (1, -1), (1, 0), (-1, 1), (-1, -1), (-1, 0), (0, 1), (0, -1)


def inside(lig, col):
    return 0 <= lig < 8 and 0 <= col < 8

def marque_et_compte(lig, col):
    nb_marques = 0
    for dl, dc in DIRECTIONS:
        x = lig + dl
        y = col + dc
        while inside(x, y):
            if grille[x][y] == LIBRE:
                grille[x][y] = EN_PRISE
                nb_marques += 1
            x += dl
            y += dc
    return nb_marques


def total_marquees():
    nb_marques = 0
    for lig in range(8):
        for col in range(8):
            if grille[lig][col] == REINE:
                nb_marques = nb_marques + 1 + marque_et_compte(lig, col)
    return nb_marques

# Programme principal
#
grille = [list(input()) for _ in range(8)]
nb_paisibles = 64 - total_marquees()
print(nb_paisibles)