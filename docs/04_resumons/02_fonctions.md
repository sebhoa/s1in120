# Les fonctions

Les fonctions que nous avons vues étaient rattachées à des objets : on les appelle des méthodes. Mais le principe est le même : réaliser une tâche en fonction d'informations fournies.

On distingue les fonctions qui calculent effectivement un résultat et celles qui réalise une action.

## Fonction sans calcul

Nous pouvons définir une fonction `positionne` qui prend une tortue en paramètre ainsi qu'une abcisse et une ordonnée et positionne la tortue au point en question, sans laisser de tracé :

```python
def positionne(tortue, x, y):
    tortue.up()
    tortue.goto(x, y)
    tortue.down()
```

## Fonction avec calcul et valeur de retour

Nous pouvons aussi vouloir calculer la distance d'une tortue au centre de la fenêtre (qui possède les coordonnées 0, 0).  Nous allons pour cela utiliser :

- la fonction `sqrt` (pour _square root_ ou racine carrée en anglais) 
- la méthode `pos` de la tortue pour obtenir sa position

```python
from math import sqrt

def distance(tortue):
    x, y = tortue.pos()
    d = sqrt(x**2 + y**2)
    return d
```

!!! warning "Attention"
    L'instruction `return` permet de retourner la valeur calculée ici la distance. C'est grâce à ce retour de valeur qu'on pourra calculer la distance et utiliser le résultat dans un autre calcul ou pour une affectation par exemple.

    En Python, toutes fonctions retournent quelque chose. Si l'instruction `return` n'est pas présente explicitement c'est la valeur `None` qui est renvoyée. C'est une valeur un peu particulière que l'on peut traduire par _néant_  ou _rien_.


## Utiliser les fonctions

!!! question "Question"
    Créez une tortue, faites lui faire un tour :
    
    - 100px vers l'Est
    - 205px à droite de 55°
    - 50px  vers l'Ouest

    A quelle distance se trouve-t-elle de son point de départ ?

??? success "Solution"

    ```python
    import turtle
    from math import sqrt

    def distance(tortue):
        x, y = tortue.pos()
        d = sqrt(x**2 + y**2)
        return d
    
    def tour(tortue):
        tortue.fd(100)
        tortue.rt(55)
        tortue.fd(205)
        tortue.rt(125)
        tortue.fd(50)
    
    caro = turtle.Turtle()
    tour(caro)
    print('Caro est a', distance(caro), 'px de son point de départ')
    ```

Quel est le lien entre les variables `caro` et `tortue` ? La distance qu'on veut calculer concerne `caro` mais au moment de recevoir comme information la Turtle `caro` la fonction `distance` va créer un _alias_ ie une 2e référence. C'est avec cette référence `tortue` que va travailler la fonction. Lorsqu'elle a fini sont calcul et qu'elle retourne son résultat, la variable `tortue` est détruite. 

!!! warning "Attention"

    Il faut beaucoup pratiquer les fonctions pour bien comprendre le mécanisme d'appel de la fonction avec les arguments (les informations) qu'on doit lui fournir.