# La notion de variable

## Les objets

Nos programmes Python manipulent des objets. Comme Turtle. Mais il y a des objets plus simples :

- `int` est l'objet des entiers positifs et négatifs. `0`, `-16`, sont des entiers. Python a la particuliarité de pouvoir travailler avec des entiers très très grands. Dans un interprète interactif vous pouvez essayer :
   ```python
   >>> 2021 ** 2021
   >>>
   ```
- `float` est l'objet des nombres à virgule... nous reviendrons sur ces nombres un peu particuliers. Pour l'instant retenez que si dans un programme vous avez le choix entre utiliser des `int` et des `float` privilégiez les `int` 
- `str` est l'objet chaîne de caractères : `'Bonjour'`, `"J'aime le Python"` sont des `str`. Les `' '` et les `"  "` qui encadrent la chaîne sont des délimiteurs pour dire à Python où commence notre chaîne et où elle finit.

Nous avons aussi vu l'objet `range` qui s'utilise avec la boucle `for`.

D'autres objets seront vus : les `bool`, les `tuple`, les `list`, les `dict`...

## Référencer les objets

Pour manipuler les objets on a souvent besoin de leur donner un nom, on dit qu'on les référence. Pour cela on se sert d'une variable qui a un nom (un **identifiant**) et on utilise l'instruction d'**affectation** (`=`) :

- Une variable `prenom` qui référence un objet `str` :

    ```python
    >>> prenom = 'Manon'
    >>>
    ```

- Une variable `taille` qui référence un objet `int` et une 2e variable qui référence le même objet :

    ```python
    >>> taille = 178
    >>> copie = taille
    >>> 
    ```
[Pythontutor](http://www.pythontutor.com/visualize.html#mode=edit) est un outil pour visualiser l'état d'un programme (on parle de diagramme d'état) :

??? example "Pythontutor"

    <iframe width="600" height="300" frameborder="0" src="http://pythontutor.com/iframe-embed.html#code=taille%20%3D%20178%0Acopie%20%3D%20taille&codeDivHeight=400&codeDivWidth=350&cumulative=false&curInstr=2&heapPrimitives=true&origin=opt-frontend.js&py=3&rawInputLstJSON=%5B%5D&textReferences=false"> </iframe>

## Les opérateurs arithmétiques

Tous les opérateurs usuels sont connus : addition (`+`), soustraction (`-`), multiplication (`*`), division entière (`\\`), division (`\`), reste de la division entière ou modulo (`%`), élévation à la puissance (`**`). Testez ces opérateurs (notez leurs priorités).



