# La rosace expliquée

Continuons à utiliser les objets, les tortues pour faire de beaux dessins et apprendre d'autres concepts du langage Python.

## Introduction

Ce projet a été inspiré par la présentation de Mickaël Launay : [La face cachée des tables de multiplication](https://youtu.be/-X49VQgi86E). En effet, quelques multiplications, un cercle et des segments suffisent pour dessiner ce type de rosaces :

??? example "Un exemple de rosace"
    ![rosace marron](../assets/images/21_360.png)


## Le principe

Le principe général est le suivant : on choisit une table de multiplication, disons 2. Puis on choisit un nombre de points, qui seront répartis de façon équilibrée sur un cercle. Prenons 10 par exemple on obtient ce cercle :

![cercle 10](../assets/images/cercle_10.png)


Puis on commence à multiplier : **1** x 2 = **2** on relie les points 1 et 2 sur le cercle, **2** x 2 = **4** on relie les points 2 et 4, **3** x 2 = **6** on relie 3 et 6... **5** x 2 = 10 ... oops il n'y a pas de point 10, non mais on va faire les calculs **modulo** le nombre de points ici 10. Donc 10 modulo 10 cela fait 0 : on relie 5 et 0, **6** x 2 = 12 mais modulo 10 cela fait **2** : on relie 6 et 2 et ainsi de suite. On obtient le cercle :

![cercle 10 segments](../assets/images/cercle_10_fini.png)

Il suffit d'augmenter les points pour obtenir une rosace (table de 2 mais 360 points) :

![rosace 2 360](../assets/images/2_360.png)

## L'objet Rosace

Nous allons donc modéliser notre rosace par un objet informatique : `Rosace`.

!!! question "Question"
    Quelles sont les propriétés, les attributs de notre objet `Rosace` ?

??? success "Solution"

    - une valeur table de multiplication, nommons `table` cette propriété
    - un nombre points sur le cercle, que nous pouvons nommer `modulo` ; ce serait bien sur ce nombre de point soit un diviseur de 360 puisqu'ils doivent être répartis sur le cercle : 10, 36, 72, 120, 180, 360 sont des valeurs possibles
    - une `couleur`
    - un `rayon` peut-être
    - une tortue pour se dessiner
    - ... nous verrons par la suite si des propriétés se révèlent, il sera toujours temps de les ajouter

Passons au chapitre suivant pour commencer à construire notre objet.
