# Notre objet Rosace

Au chapitre précédent, nous nous sommes arrêtés sur les propriétés d'un objet Rosace : 

- une valeur table de multiplication, nommons `table` cette propriété
- un nombre points sur le cercle, que nous pouvons nommer `modulo` ; ce serait bien sur ce nombre de point soit un diviseur de 360 puisqu'ils doivent être répartis sur le cercle : 10, 36, 72, 120, 180, 360 sont des valeurs possibles
- une `couleur`
- un `rayon` 
- une `tortue`

## Création

```python
class Rosace:

    def __init__(self, table=2, modulo=360, rayon=200, couleur='black'):
        self.table = table
        self.modulo = modulo
        self.rayon = rayon
        self.couleur = couleur
        self.tortue = turtle.Turtle()
```

!!! notation
    Dans les paramètres du constructeur, nous avons mis des valeurs par défaut aux divers paramètres. Ainsi, lorsqu'on note `table=2` cela signifie qu'en cas d'absence de l'argument au monent de l'appel, c'est la valeur 2 qui sera prise. C'est pratique notamment quand il y a beaucoup de paramètres, pour pouvoir omettre certains qui prendront donc des valeurs par défaut. 

    Voici quelques exemple d'utilisations :

    ```python
    >>> rosace_par_defaut_noire = Rosace()
    >>> rosace_table_21_bleu = Rosace(21, couleur='blue')
    >>> couleur_alea = randint(0, 255), randint(0, 255), randint(0, 255)
    >>> rosace_aleatoire = Rosace(randint(2,100), couleur=couleur_alea)
    ```

## Les méthodes

Notre Rosace doit pouvoir se dessiner c'est-à-dire :

1. dessiner le cercle avec les points espacés régulièrement
2. dessiner les segments

La trsduction de cette méthode est donc simple :

```python
def draw(self):
    self.circle()
    self.segments()
```

### Dessiner le cercle

Il s'agit de dessiner le cercle du bon rayon et surtout y _mettre_ les points espacés comme il faut. Alors enr éalité nous n'allons pas mettre les points comme dans la version de l'introduction où nous les avions mis ainsi que les numéros pour les besoins de l'explication. 

Là, nous allons simplement mémoriser les coordonnées dans un tableau, un objet `list` en Python pour pouvoir ensuite les utiliser pour tracer les segments. Cette `list` sera une propriété de notre Rosace que nous appelerons `points`.

La méthode `circle` du module turtle permet de dessiner un **arc** de cercle : il suffit de donner le rayon en premier argument et l'angle duquel on vet tourner en deuxième argument. 

!!! question "Question"
    De quel angle doit-on tourner ici ?

??? success "Solution"
    Oui de 360 divisé par le nombre de points ie `modulo`.

Une fois notre petit arc dessiné, il nous faut mémoriser l'emplacement de la tortue : la méthode `pos` du module turtle nous donne cette information. Ces coordonnées devrons être ajoutées à une `list`. Une `list` en Python est un objet permettant de manipuler plusieurs valeurs aisément. Voici une liste (on dit aussi tableau) d'entiers :

```python
>>> ma_liste = [16, -7, 0, 1]
```

La méthode `append` des objets `list` permet de rajouter un élément à la fin de la liste :

```python
>>> ma_liste.append(-2)
>>> ma_liste
[16, -7, 0, 1, -2]
```

Notre méthode `circle` peut commencer à s'écrire :

```python
def circle(self):
    angle = 360 // self.modulo
    for _ in self.modulo:
        self.points.append(self.tortue.pos())
        self.tortue.circle(self.rayon, angle)
```

Tapez le code que nous avons déjà mis en place (le constructeur en n'oubliant pas la propriété `self.points` qui doit être initilisée à la liste vide : `[]`) puis testez : créez une Rosace et faites dessiner le cercle.

!!! question "Question"
    Ne trouvez-vous pas que le cercle est décalé vers le haut ? Trouvez pourquoi et proposer une correction.

??? success "Solution"
    La méthode `circle` de turtle dessine un cercle **à la gauche** de la tortue : ainsi, quand la tortue est déjà au centre de l'écran et regarde vers l'Est, le cercle se dessine au-dessus.

    On peut commencer notre méthode `circle` par positionner la tortue un peu vers le bas, de `self.rayon / 2` pour être précis.

### Dessiner les segments

Il faut _réciter_ la table de multiplication depuis 1 jusqu'au dernier points (ie `self.modulo - 1`) et faire cette multiplication modulo, notre modulo. Concrètement, on fait une boucle `for i in range(1, self.modulo)` :

- récupèrons les coordonnées du point d'indice `i`
- amenons la tortue à ce point, sans laisser de trace
- effectuons la multiplication qui nous donne un autre indice 
- rendons nous à cet autre point cette fois en laissant une ligne : nous avons dessiné notre premier segment
- Nous recommençons avec le `i` suivant.


```python
def segments(self):
    for i in range(1, self.modulo):
        x, y = self.points[i]
        self.aller(x, y)
        n = (self.table * i) % self.modulo
        x, y = self.points[n]
        self.aller(x, y, trace=True)
```

Voilà il ne vous reste plus qu'à regrouper l'ensemble du code dans un fichier `rosace.py` à compléter par un programme principal qui créé une rosace aléatoire par exemple.

![rosace](../assets/images/rosace.png)