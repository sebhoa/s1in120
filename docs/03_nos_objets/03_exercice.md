# Art moderne

![composition C](../assets/images/mondrian_CIII.jpg){align=left} [Piet Mondiran](https://fr.wikipedia.org/wiki/Piet_Mondrian) est un peintre néerlandais de la fin du 19e début du 20e siècle qui a notamment réalisé des oeuvres d'art abstrait. 

Nous nous proposons d'utiliser notre objet `Carre` pour créer un tableau contenant juste 3 carrés de dimensions différentes : un jaune, un rouge et un bleu.

??? tip "Astuce"
    Pensez à modifier notre classe `Carre` pour prendre en compte la propriété `couleur` du carré. La méthode `se_positionne` fonctionne-t-elle toujours avec plusieurs carrés ?

    Ce serait plus joli ou en tout cas plus _ressemblant_ à l'esprit des tableau de P. Monrian si les carrés étaient pleins.

    La documentation officielle de _turtle_ se trouve ici : [documentation Python](https://docs.python.org/fr/3/library/turtle.html). Cherchez-y des méthodes pouvant vous aider.

??? success "Solution"

    Il faut redéfinir la méthode pour se positionner en utilisant la méthode `goto` de `turtle`. Il faut aussi remplir le carré avec `begin_fill` et `end_fill`.

    La fonction `randint` du module `random` permet d'obtenir un entier aléatoire entre 2 valeurs données. Cela permet d'obtenir des tableaux différents à chaque exécution du programme.
    
    ```python
    import turtle
    from random import randint

    class Carre:

        tortue = turtle.Turtle()

        def __init__(self, x, y, cote, couleur):
            self.pos = x, y
            self.cote = cote
            self.couleur = couleur
            self.t = Carre.tortue
            self.t.ht()
    
        def se_positionne(self):
            x, y = self.pos
            self.t.up()
            self.t.goto(x, y)
            self.t.down()

        def se_dessine(self):
            self.se_positionne()
            self.t.color(self.couleur)
            self.t.begin_fill()
            for _ in range(4):
                self.t.fd(self.cote)
                self.t.lt(90)
            self.t.end_fill()    
        
    # programme principal

    rouge = Carre(randint(-400, -200), randint(0, 50), randint(250, 400), 'darkred')
    bleu = Carre(randint(-350, 100), randint(-350, -100), randint(100, 180), 'darkblue')
    jaune = Carre(randint(0, 100), randint(0, 100), randint(180, 250), 'gold')
    rouge.se_dessine()
    bleu.se_dessine()
    jaune.se_dessine()

    turtle.mainloop()
    ```

![art abstrait](../assets/images/art_abstrait.png)
