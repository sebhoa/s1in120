# Factoriser du code

- Eviter d'écrire plusieurs fois les mêmes instructions ou des instructions similaires
- Regrouper dans une fonction les instructions qui résolvent une tâche bien précise

Réviser ce qui a été présenté dans le [chapitre Python](https://sebhoa.gitlab.io/s1sc122/02_Python/intro/) de l'UE précédente.

    
## Retour aux méthodes

Maintenant que nous avons un moyen de répéter les instructions, terminons notre méthode pour dessiner. Juste un mot sur le positionnement. Il s'agit d'aller **sans laisser de trace** au point de coordonnées définies par la propriété `pos`. Définissons une méthode `se_positionne()` :

```python
def se_positionne(self):
    x, y = self.pos
    self.t.up()
    self.t.lt(180)
    self.fd(x)
    self.lt(90)
    self.fd(y)
    self.lt(90)
```

```python
def se_dessine(self):
    self.se_positionne()
    for _ in range(4):
        self.t.fd(self.cote)
        self.t.lt(90)
```
Et voici à quoi ressemble notre nouveau programme `carre.py`:

??? success "carre.py"
    
    ```python
    import turtle

    class Carre:

        def __init__(self, x, y, cote, tortue):
            self.pos = x, y
            self.cote = cote
            self.t = tortue

        def se_positionne(self):
            x, y = self.pos
            self.t.up()
            self.t.lt(180)
            self.t.fd(x)
            self.t.lt(90)
            self.t.fd(y)
            self.t.lt(90)
            self.t.down()

        def se_dessine(self):
            self.se_positionne()
            for _ in range(4):
                self.t.fd(self.cote)
                self.t.lt(90)
    
    # Le programme principal

    carre = Carre(0, 0, 150, turtle.Turtle())
    carre.se_dessine()

    turtle.mainloop()
    ```

Ainsi si nous voulons créer un autre carré, en 20, 50 et taille 200 :

```python
autre_carre = Carre(20, 50, 200, turtle.Turtle())
```

Mais une petite minute... à chaque carré créé, nous créons une tortue ? Une tortue peut certainement dessiner plusieurs carrés et ça semble un peu surdimensionné de devoir en créer une par carré. Voyons comment n'avoir qu'une tortue pour tous nos carrés.


!!! warning "Éléments de syntaxe à ne pas oublier"
    - Ne pas oublier les `:` lors des définitions de méthodes ou de classes et dans les boucles. Ces lignes s'appellent des **entêtes** et indiquent qu'il y aura un **corps** 
    - Ne pas oublier l'indentation du bloc d'instructions qui arrive après une ligne d'entête.
    - Ne pas oublier `turtle.mainloop()` à la fin de votre programme manipulant des _turtle_ 


## Un attribut de classe

La propriété `t` qui est un objet Turtle n'a pas besoin d'être différente pour chaque carré créé. Voici une solution :

```python
class Carre:

    tortue = turtle.Turtle()

    def __init__(self, x, y, cote):
        self.pos = x, y
        self.cote = cote
        self.t = Carre.tortue
    
    ...
```

Nous avons créé une `tortue` au niveau de la classe. Chaque instance définit un _alias_ c'est-à-dire une nouvelle référence vers le même unique objet Turtle. Vous vous souvenez, comme `caro` était un _alias_ de `caroline`.

!!! note
    Lorsqu'on définit une propriété `prop` à l'extérieure du constructeur d'une classe `A`, il s'agit d'une propriété de classe et s'utilise ainsi : `A.prop`

 