![logo](assets/images/logo_rosace.svg) 

# Programmer en Python

Site support de l'UE du même nom. 

**Pré-requis :** [Partie Informatique de l'UE de Méthodologie Scientifique](https://sebhoa.gitlab.io/s1sc122/). On y trouvera notamment toutes les installations à faire.
