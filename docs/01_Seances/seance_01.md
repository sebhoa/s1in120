# 01

**Dates :** mardi 5.10 8h30 et jeudi 7.10 14h

## Contenu

- présentation de l'UE
- tour d'horizon des concepts étudiés :
    - listes, tuples, dictionnaires : analyse et amélioration d'une solution à l'exercice 3.7 du Mooc
    - fichiers, redirections : sudoku
    - modules : csv, json etc. 
    - programmation objet
